import Examen.ejercicio2.GestionVehiculos;
import Examen.ejercicio2.Vehiculo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GestionVehiculosTest {
    GestionVehiculos gest = new GestionVehiculos();
    Vehiculo vehiculo1 = new Vehiculo("Modelo1","Marca1",1234);
    Vehiculo vehiculo2 = new Vehiculo("Modelo2","Marca2",4321);
    Vehiculo vehiculoNulo = new Vehiculo("","",0);
    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }
//AÑADIR VEHICULO
    @Test
    void anadirVehiculo_feliz() {
        gest.anadirVehiculo(vehiculo1);
        Assertions.assertNotNull(gest.buscarVehiculo("Modelo1"));
    }
    @Test
    void anadirVehiculo_dos(){
        gest.anadirVehiculo(vehiculo1);
        Vehiculo vehiculoExp = gest.buscarVehiculo("Modelo1");
        gest.anadirVehiculo(vehiculo1);
        Assertions.assertEquals(vehiculoExp,gest.buscarVehiculo("Modelo1"));
    }
    @Test
    void anadirVehiculo_nulo(){
        gest.anadirVehiculo(vehiculoNulo);
        Assertions.assertNull(gest.buscarVehiculo(""));
    }
//ELIMINAR VEHICULO
    @Test
    void eliminarVehiculo() {
        gest.anadirVehiculo(vehiculo1);
        gest.eliminarVehiculo("Modelo1");
        Assertions.assertNull(gest.buscarVehiculo("Modelo1"));
    }

    @Test
    void eliminarVehiculo_nulo(){
        gest.anadirVehiculo(vehiculoNulo);
        gest.eliminarVehiculo("");
        Assertions.assertNull(gest.buscarVehiculo(""));
    }

//BUSCAR VEHICULO
    @Test
    void buscarVehiculo() {
        gest.anadirVehiculo(vehiculo1);
        Assertions.assertEquals(vehiculo1,gest.buscarVehiculo("Modelo1"));
    }

    @Test
    void buscarVehiculoinex(){
        Assertions.assertNull(gest.buscarVehiculo("Modelo1"));
    }

    @Test
    void buscarVehiculoNulo(){
        gest.anadirVehiculo(vehiculoNulo);
        Assertions.assertNull(gest.buscarVehiculo(""));
    }
}