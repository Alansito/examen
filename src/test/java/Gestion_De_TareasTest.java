import Examen.ejercicio1.Gestion_De_Tareas;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Gestion_De_TareasTest {

    Gestion_De_Tareas gest = new Gestion_De_Tareas();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
    /*METODO AÑADIR TAREA*/

    @Test
    void add_tarea_Feliz() {
        Assertions.assertTrue(gest.add_Tarea(1,"Tarea1","Descripcion1","01/01"));
    }
    @Test
    void add_tarea_repetida(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertFalse(gest.add_Tarea(1,"Tarea1","Descripcion1","01/01"));
    }
    /*METODO ELIMINAR TAREA*/

    @Test
    void rm_tarea_feliz(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertTrue(gest.rm_Tarea(1));
    }
    @Test
    void rm_tarea_repetida(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.rm_Tarea(1);
        Assertions.assertFalse(gest.rm_Tarea(1));
    }
    @Test
    void rm_tarea_inexistente(){
        Assertions.assertFalse(gest.rm_Tarea(1));
    }

    /*METODO BUSCAR TAREA*/

    @Test
    void buscar_tarea_feliz(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertEquals(gest.tareasMap.get(1),gest.buscarTarea(1));
    }
    @Test
    void buscar_tarea_inex(){
        Assertions.assertEquals(null,gest.buscarTarea(1));
    }
    @Test
    void buscar_tarea_elim(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.rm_Tarea(1);
        Assertions.assertEquals(null,gest.buscarTarea(1));
    }

    /*COMPLETAR TAREA*/
    @Test
    void completar_feliz(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertTrue(gest.completada(1));
    }
    @Test
    void completar_tarea_pend(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertTrue(gest.completada(1));
    }
    @Test
    void completar_tarea_prog(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertTrue(gest.completada(1));
    }
    @Test
    void completar_inex(){
        Assertions.assertFalse(gest.completada(1));
    }
    @Test
    void completar_inex_elim(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.rm_Tarea(1);
        Assertions.assertFalse(gest.completada(1));
    }
    /*TAREA PENDIENTE*/
    @Test
    void pend_feliz(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertTrue(gest.pendiente(1));
    }
    @Test
    void pend_comp(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.completada(1);
        Assertions.assertTrue(gest.pendiente(1));
    }
    @Test
    void pend_prog(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.enProgreso(1);
        Assertions.assertTrue((gest.pendiente(1)));
    }
    @Test
    void pend_inex(){
        Assertions.assertFalse(gest.pendiente(1));
    }
    @Test
    void pend_inex_elim(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.rm_Tarea(1);
        Assertions.assertFalse(gest.pendiente(1));
    }
    /*TAREA EN PROGRESO*/
    @Test
    void prog_feliz(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertTrue(gest.enProgreso(1));
    }
    @Test
    void prog_comp(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.completada(1);
        Assertions.assertTrue(gest.enProgreso(1));
    }
    @Test
    void prog_pend(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.pendiente(1);
        Assertions.assertTrue(gest.enProgreso(1));
    }
    @Test
    void prog_inex(){
        Assertions.assertFalse(gest.enProgreso(1));
    }
    @Test
    void prog_inex_elim(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.rm_Tarea(1);
        Assertions.assertFalse(gest.enProgreso(1));
    }
    /*CATEGORIAS*/
    @Test
    void cat_feliz(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertTrue(gest.asignCat(1,"Cat1"));
    }
    @Test
    void cat_rep(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.asignCat(1,"Cat1");
        Assertions.assertFalse(gest.asignCat(1,"Cat1"));
    }
    @Test
    void cat_doble (){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        gest.asignCat(1,"Cat1");
        Assertions.assertTrue((gest.asignCat(1,"Cat2")));
    }
    @Test
    void cat_inex(){
        gest.add_Tarea(1,"Tarea1","Descripcion1","01/01");
        Assertions.assertFalse(gest.asignCat(2,"Cat1"));
    }
}