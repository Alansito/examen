package Examen.ejercicio1;

public class Tareas {
    public int id;
    public String titulo;
    public String dsc;
    public String fecha;
    public boolean completado;
    public boolean progreso;
    public boolean pendiente;

    public Tareas (int id, String titulo, String dsc, String fecha){
        this.id=id;
        this.titulo=titulo;
        this.dsc=dsc;
        this.fecha=fecha;
        this.completado=false;
        this.progreso=false;
        this.pendiente=false;
    }

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDsc() {
        return dsc;
    }

    public String getFecha() {
        return fecha;
    }

    public boolean isCompletado() {
        return completado;
    }

    public boolean isProgreso() {
        return progreso;
    }

    public boolean isPendiente() {
        return pendiente;
    }

    public void setCompletado(boolean completado) {
        this.completado = completado;
    }

    public void setProgreso(boolean progreso) {
        this.progreso = progreso;
    }

    public void setPendiente(boolean pendiente) {
        this.pendiente = pendiente;
    }

    public String toString(){
        if(isCompletado()){
            return "Tarea [Id= "+id+" | Titulo: "+titulo+ " | Descripcion: "+dsc+" | Fecha: "+fecha+" | Estado: Completado]";
        }
        if(isProgreso()){
            return "Tarea [Id= "+id+" | Titulo: "+titulo+ " | Descripcion: "+dsc+" | Fecha: "+fecha+" | Estado: En progreso]";
        }
        if(isPendiente()){
            return "Tarea [Id= "+id+" | Titulo: "+titulo+ " | Descripcion: "+dsc+" | Fecha: "+fecha+" | Estado: Pendiente]";
        }
        return "Tarea [Id= "+id+" | Titulo: "+titulo+ " | Descripcion: "+dsc+" | Fecha: "+fecha+" | Estado: Pendiente]";
    }
}
