package Examen.ejercicio1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Gestion_De_Tareas {
    public Map<Integer, Tareas> tareasMap = new HashMap<>();
    public List<Categorias> categoriasList=new ArrayList<>();
    public HashMap<String,Categorias>categoriasMap=new HashMap<>();


    public boolean add_Tarea(int id, String titulo, String dsc, String fecha){
        if(tareasMap.containsKey(id)){
            return false;
        }
        tareasMap.put(id, new Tareas(id, titulo, dsc, fecha));
        return true;
    }

    public boolean rm_Tarea (int id){
        if(tareasMap.containsKey(id)){
            tareasMap.remove(id);
            return true;
        }
        return false;
    }

    public Tareas buscarTarea(int id){
        if(!tareasMap.containsKey(id)){
            System.out.println("TAREA NO ENCONTRADA");
            return tareasMap.get(id);
        }
        return tareasMap.get(id);
    }

    public boolean completada (int id){
        if(tareasMap.get(id)==null){
            return false;
        }
        tareasMap.get(id).setCompletado(true);
        tareasMap.get(id).setPendiente(false);
        tareasMap.get(id).setProgreso(false);
        return true;
    }

    public boolean pendiente(int id){
        if(tareasMap.get(id)==null){
            return false;
        }
        tareasMap.get(id).setCompletado(false);
        tareasMap.get(id).setPendiente(true);
        tareasMap.get(id).setProgreso(false);
        return true;
    }

    public boolean enProgreso(int id){
        if(tareasMap.get(id)==null){
            return false;
        }
        tareasMap.get(id).setCompletado(false);
        tareasMap.get(id).setPendiente(false);
        tareasMap.get(id).setProgreso(true);
        return true;
    }
    public Categorias buscarCat (String nombreCat){
        return categoriasMap.get(nombreCat);
    }

    public boolean asignCat(int id, String nombreCat){
        Tareas tareas = tareasMap.get(id);
        if(!tareasMap.containsKey(id)){
            return false;
        }
        categoriasMap.put(nombreCat,new Categorias(tareas.getId(),tareas.getTitulo(),tareas.getDsc(),tareas.getFecha(),nombreCat));
        return true;
    }
}
