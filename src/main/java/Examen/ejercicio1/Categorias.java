package Examen.ejercicio1;


public class Categorias extends Tareas {
    String nombreCat;
    public Categorias(int id, String titulo, String dsc, String fecha, String nombreCat) {
        super(id, titulo, dsc, fecha);
        this.nombreCat = nombreCat;
    }
    public String toString(){
        return "Tarea [Id= "+id+" | Titulo: "+titulo+ " | Descripcion: "+dsc+" | Fecha: "+fecha+" | Categoria: "+nombreCat+"]";
    }
}
